# mpl

パズル用LaTeXコードを生成するソフトウェアです

# ビルド方法

ビルドは

```
cargo build
```

で、`arget/debug/mpl.exe`ファイルが作られる。

# インストール方法

```
cargo install --path .
```

でインストールできる。

# 起動方法

```
mpl <csvファイル> -o <出力先のファイル名> -c <LaTeXのコマンド名>
```

です。

つまり、

```
mpl test/test.csv -o test/test.tex -c test
```

とすると、「testフォルダ内のtest.csvファイルを入力してtestフォルダ内にtest.texファイルを作る。そして、そこで作られるLaTeXのコマンド名は`\testQ`と`\testA`である」となる。


テスト時は

```
cargo build
```

で作り、

```
target/debug/mpl <csvファイル> -o <出力先のファイル名> -c <LaTeXのコマンド名>
```

で新しいもので試すことができる。

# CSVファイルについて

「サイズ, path, 作者, レベル」で記入する
一行目は読み込まれないので、「サイズ, path, 作者, レベル」を書くことをおススメする。

どれかが欠けたりすると実行時エラーになり。

# testフォルダについて

testフォルダ内に、実行例があります。

testフォルダ内の`niporiTest.sty`の中は生成されるLaTeXファイルを処理できるようにコマンドが定義されています。
`nipori.sty`にコピーして本番に使ってください。
