// ライブラリの読み込み
use clap::{App, Arg};
use std::fs::File;
use std::io::prelude::*;

mod make_latex_code;

// ファイルを書き出す関数
fn write_file(file_name: String, text: String) {
  let mut file = File::create(file_name).unwrap();
  file.write_all(text.as_bytes()).unwrap();
}

fn main() {
  // 引数等の指定
  let app = App::new("mpl")
    .version("0.0.1")
    .arg(
      // csvファイルのファイル名を受け取る
      Arg::with_name("input")
        .help("Specify input file")
        .value_name("FILE")
        .takes_value(true),
    )
    .arg(
      // 出力するlatexファイルのファイル名を受け取る
      Arg::with_name("output")
        .help("Specify output file")
        .value_name("FILE")
        .short("o")
        .long("output")
        .takes_value(true),
    )
    .arg(
      // 定義するコマンド名を指定する
      Arg::with_name("command_name")
        .help("Specify command name")
        .value_name("STRING")
        .short("c")
        .long("command")
        .takes_value(true),
    );

  // 引数の値を取り出す
  let matches = app.get_matches();
  let input_file_name_opt = matches.value_of("input");
  let output_file_name_opt = matches.value_of("output");
  let command_name_opt = matches.value_of("command_name");

  match (input_file_name_opt, output_file_name_opt, command_name_opt) {
    // 全てある場合は処理
    (Some(input_file_name), Some(output_file_name), Some(command_name)) => {
      // from_path関数を使ってpathからデータを取り出す
      let mut csv_reader = csv::Reader::from_path(input_file_name).unwrap();
      // for文を使ってリスト化
      let mut csv_data_vec = vec![];
      for csv_data in csv_reader.records() {
        csv_data_vec.push(csv_data.unwrap())
      }
      // make_latex_codeモジュール内のmake_latex_code関数にデータのリストを渡して
      // latexのcodeの文字列にする
      let output_string = make_latex_code::main(csv_data_vec, command_name.to_owned());
      // 生成したlatexのcodeを上の方で作成したwrite_file関数を使って書き出す
      write_file(output_file_name.to_owned(), output_string)
    }
    // 何かが指定されていなかったらエラー
    _ => eprintln!("引数が何か足りていません"),
  }
}
