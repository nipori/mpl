// パズルを出力するコードに必要な情報
#[derive(Debug, Clone)]
struct Puzzle {
  size: f64,
  path: String,
  author: String,
  level: String,
}

// 各パズルの横幅を足してどの値にするかの定数
// 1.0にすると\minipageコマンドの影響で溢れてしまう
const MAX_WIDTH: f64 = 0.95;

pub fn main(csv_data: Vec<csv::StringRecord>, command_name: String) -> String {
  let mut puzzle_data_vec = Vec::new();
  for csv_data in csv_data.iter() {
    // getメソッドを使うと文字列を取り出せる
    // https://docs.rs/csv/1.1.3/csv/struct.StringRecord.html#method.get
    let size_str = csv_data.get(0).unwrap();
    let path_str = csv_data.get(1).unwrap();
    let author_str = csv_data.get(2).unwrap();
    let level_str = csv_data.get(3).unwrap();
    // puzzle型にしてリストにする
    puzzle_data_vec.push(Puzzle {
      size: size_str.parse().unwrap(),
      path: path_str.to_owned(),
      author: author_str.to_owned(),
      level: level_str.to_owned(),
    })
  }
  // 問題用と解答用の文字列をそれぞれ生成する
  let q_string = latex_code_q(&puzzle_data_vec);
  let a_string = latex_code_a(&puzzle_data_vec);
  // 出来た文字列を組み合わせて
  // \suririnQ や \suririnA のようなコマンドを定義する文字列を生成する
  format!(
    "
\\newcommand{{\\{}Q}}{{
  {}
}}\n\n
\\newcommand{{\\{}A}}{{
  {}
}}
",
    command_name, q_string, command_name, a_string
  )
}

// 問題部分を生成する
// sizeの和が2.3を超えたら単独
// 2.3という数字は適度に変えてヨシ
fn latex_code_q(puzzle_vec: &[Puzzle]) -> String {
  let mut code_string = String::new();
  let mut stack = vec![];
  for puzzle in puzzle_vec.iter() {
    if stack.is_empty() {
      // stackの中が何もないのでとりあえず突っ込む
      stack.push(puzzle)
    } else {
      // stackの中身を取り出してsizeの和を見てみる
      let stack_puzzle = stack[0];
      let stack_puzzle_size = stack_puzzle.size;
      let puzzle_size = puzzle.size;
      let size_sum = stack_puzzle_size + puzzle_size;
      // 和が2.3以上ならstackしていた方を単独で文字列化し、
      // 今取り出した方はstackに突っ込む
      // 和が2.3未満なら2つを文字列化
      // stackの方は空に
      if size_sum >= 2.3 {
        code_string.push_str(&format!(
          "
  \\puzzleOne{{
    {}
  }}",
          make_puzzle_q_command_str(stack_puzzle)
        ));
        stack = vec![puzzle];
      } else {
        let size1 = stack_puzzle.size;
        let size2 = puzzle.size;
        let hsize1 = (MAX_WIDTH / (size1 + size2)) * size1;
        let hsize2 = (MAX_WIDTH / (size1 + size2)) * size2;
        code_string.push_str(&format!(
          "
  \\puzzleTwo{{{}}}{{{}}}{{
    {}
  }}{{
    {}
  }}",
          hsize1,
          hsize2,
          make_puzzle_q_command_str(stack_puzzle),
          make_puzzle_q_command_str(puzzle),
        ));
        stack = vec![];
      }
    }
  }
  // stackの中に残ってたら文字列化
  match stack.len() {
    1 => {
      let puzzle = stack[0];
      code_string.push_str(&format!(
        "
  \\puzzleOne{{
    {}
  }}",
        make_puzzle_q_command_str(puzzle)
      ))
    }
    2 => {
      let puzzle1 = stack[0];
      let puzzle2 = stack[1];
      let size1 = puzzle1.size;
      let size2 = puzzle2.size;
      let hsize1 = (MAX_WIDTH / (size1 + size2)) * size1;
      let hsize2 = (MAX_WIDTH / (size1 + size2)) * size2;
      code_string.push_str(&format!(
        "
  \\puzzleTwo{{{}}}{{{}}}{{
    {}
  }}{{
    {}
  }}",
        hsize1,
        hsize2,
        make_puzzle_q_command_str(puzzle1),
        make_puzzle_q_command_str(puzzle2),
      ))
    }
    _ => {
      // 何もないと判定して何もしない
    }
  };
  code_string
}

// 解答部分を生成する
// sizeの和が4.3を超えたら単独
// 4.3という数字は適度に変えてヨシ
fn latex_code_a(puzzle_vec: &[Puzzle]) -> String {
  let mut code_string = String::new();
  let mut stack = vec![];
  for puzzle in puzzle_vec.iter() {
    if stack.is_empty() {
      // stackの中が何もないのでとりあえず突っ込む
      stack.push(puzzle)
    } else {
      // stackの和を見てみる
      let stack_size_sum = stack.iter().fold(0.0, |sum, puzzle| sum + puzzle.size);
      // stackの和と今取得したパズルのsizeの和が4.3を超えなかったら、stackにpush
      // 4.3を超えていたら、stackの中身を文字列化して、取得したものでstackを上書き
      if stack_size_sum + puzzle.size < 4.3 {
        stack.push(puzzle)
      } else {
        match stack.len() {
          1 => {
            let puzzle = stack[0];
            code_string.push_str(&format!(
              "
        \\puzzleOne{{
          {}
        }}",
              make_puzzle_a_command_str(puzzle)
            ))
          }
          2 => {
            let puzzle1 = stack[0];
            let puzzle2 = stack[1];
            let size1 = puzzle1.size;
            let size2 = puzzle2.size;
            let hsize1 = (MAX_WIDTH / (size1 + size2)) * size1;
            let hsize2 = (MAX_WIDTH / (size1 + size2)) * size2;
            code_string.push_str(&format!(
              "
        \\puzzleTwo{{{}}}{{{}}}{{
          {}
        }}{{
          {}
        }}",
              hsize1,
              hsize2,
              make_puzzle_a_command_str(puzzle1),
              make_puzzle_a_command_str(puzzle2)
            ))
          }
          3 => {
            let puzzle1 = stack[0];
            let puzzle2 = stack[1];
            let puzzle3 = stack[2];
            let size1 = puzzle1.size;
            let size2 = puzzle2.size;
            let size3 = puzzle3.size;
            let hsize1 = (MAX_WIDTH / (size1 + size2 + size3)) * size1;
            let hsize2 = (MAX_WIDTH / (size1 + size2 + size3)) * size2;
            let hsize3 = (MAX_WIDTH / (size1 + size2 + size3)) * size3;
            code_string.push_str(&format!(
              "
        \\puzzleThree{{{}}}{{{}}}{{{}}}{{
          {}
        }}{{
          {}
        }}{{
          {}
        }}",
              hsize1,
              hsize2,
              hsize3,
              make_puzzle_a_command_str(puzzle1),
              make_puzzle_a_command_str(puzzle2),
              make_puzzle_a_command_str(puzzle3)
            ))
          }
          _ => {
            // 4つあると判断して文字列化
            let puzzle1 = stack[0];
            let puzzle2 = stack[1];
            let puzzle3 = stack[2];
            let puzzle4 = stack[3];
            let size1 = puzzle1.size;
            let size2 = puzzle2.size;
            let size3 = puzzle3.size;
            let size4 = puzzle4.size;
            let hsize1 = (MAX_WIDTH / (size1 + size2 + size3 + size4)) * size1;
            let hsize2 = (MAX_WIDTH / (size1 + size2 + size3 + size4)) * size2;
            let hsize3 = (MAX_WIDTH / (size1 + size2 + size3 + size4)) * size3;
            let hsize4 = (MAX_WIDTH / (size1 + size2 + size3 + size4)) * size4;
            code_string.push_str(&format!(
              "
        \\puzzleFour{{{}}}{{{}}}{{{}}}{{{}}}{{
          {}
        }}{{
          {}
        }}{{
          {}
        }}{{
          {}
        }}",
              hsize1,
              hsize2,
              hsize3,
              hsize4,
              make_puzzle_a_command_str(puzzle1),
              make_puzzle_a_command_str(puzzle2),
              make_puzzle_a_command_str(puzzle3),
              make_puzzle_a_command_str(puzzle4)
            ))
          }
        };
        stack = vec![puzzle]
      }
    }
  }
  // stackの中に残ってたら文字列化
  match stack.len() {
    1 => {
      let puzzle = stack[0];
      code_string.push_str(&format!(
        "
  \\puzzleOne{{
    {}
  }}",
        make_puzzle_a_command_str(puzzle)
      ))
    }
    2 => {
      let puzzle1 = stack[0];
      let puzzle2 = stack[1];
      let size1 = puzzle1.size;
      let size2 = puzzle2.size;
      let hsize1 = (1.0 / (size1 + size2)) * size1;
      let hsize2 = (1.0 / (size1 + size2)) * size2;
      code_string.push_str(&format!(
        "
  \\puzzleTwo{{{}}}{{{}}}{{
    {}
  }}{{
    {}
  }}",
        hsize1,
        hsize2,
        make_puzzle_a_command_str(puzzle1),
        make_puzzle_a_command_str(puzzle2)
      ))
    }
    3 => {
      let puzzle1 = stack[0];
      let puzzle2 = stack[1];
      let puzzle3 = stack[2];
      let size1 = puzzle1.size;
      let size2 = puzzle2.size;
      let size3 = puzzle3.size;
      let hsize1 = (1.0 / (size1 + size2 + size3)) * size1;
      let hsize2 = (1.0 / (size1 + size2 + size3)) * size2;
      let hsize3 = (1.0 / (size1 + size2 + size3)) * size3;
      code_string.push_str(&format!(
        "
  \\puzzleThree{{{}}}{{{}}}{{{}}}{{
    {}
  }}{{
    {}
  }}{{
    {}
  }}",
        hsize1,
        hsize2,
        hsize3,
        make_puzzle_a_command_str(puzzle1),
        make_puzzle_a_command_str(puzzle2),
        make_puzzle_a_command_str(puzzle3)
      ))
    }
    _ => {
      // 4つあると判断して文字列化
      let puzzle1 = stack[0];
      let puzzle2 = stack[1];
      let puzzle3 = stack[2];
      let puzzle4 = stack[3];
      let size1 = puzzle1.size;
      let size2 = puzzle2.size;
      let size3 = puzzle3.size;
      let size4 = puzzle4.size;
      let hsize1 = (1.0 / (size1 + size2 + size3 + size4)) * size1;
      let hsize2 = (1.0 / (size1 + size2 + size3 + size4)) * size2;
      let hsize3 = (1.0 / (size1 + size2 + size3 + size4)) * size3;
      let hsize4 = (1.0 / (size1 + size2 + size3 + size4)) * size4;
      code_string.push_str(&format!(
        "
  \\puzzleFour{{{}}}{{{}}}{{{}}}{{{}}}{{
    {}
  }}{{
    {}
  }}{{
    {}
  }}{{
    {}
  }}",
        hsize1,
        hsize2,
        hsize3,
        hsize4,
        make_puzzle_a_command_str(puzzle1),
        make_puzzle_a_command_str(puzzle2),
        make_puzzle_a_command_str(puzzle3),
        make_puzzle_a_command_str(puzzle4)
      ))
    }
  };
  code_string
}

// \puzzleQコマンドを生成する関数
fn make_puzzle_q_command_str(puzzle: &Puzzle) -> String {
  format!(
    "\\puzzleQ{{{}}}{{{}-Q.jpeg}}{{{}}}{{{}}}",
    puzzle.size, puzzle.path, puzzle.author, puzzle.level
  )
}

// \puzzleQコマンドを生成する関数
fn make_puzzle_a_command_str(puzzle: &Puzzle) -> String {
  format!(
    "\\puzzleA{{{}}}{{{}-A.jpeg}}{{{}}}{{{}}}",
    puzzle.size, puzzle.path, puzzle.author, puzzle.level
  )
}
